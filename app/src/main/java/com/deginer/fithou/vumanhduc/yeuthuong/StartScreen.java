package com.deginer.fithou.vumanhduc.yeuthuong;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

/**
 * Created by VuManhDuc on 26/03/2017.
 */

public class StartScreen extends Activity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_screen);

// Tao thread hien man hinh chao
        Thread timeThread = new Thread(){
            public void run(){
                try {
                    sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                finally {
                    Intent intent = new Intent(StartScreen.this,MainActivity.class);
                    startActivity(intent);
                }
            }
        };
        timeThread.start();
    }
    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }
}
