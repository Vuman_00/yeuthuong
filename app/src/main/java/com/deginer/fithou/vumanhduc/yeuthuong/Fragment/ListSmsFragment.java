package com.deginer.fithou.vumanhduc.yeuthuong.Fragment;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.text.ClipboardManager;
import android.text.Html;
import android.text.Spanned;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.deginer.fithou.vumanhduc.yeuthuong.Adapters.SmsAdapter;
import com.deginer.fithou.vumanhduc.yeuthuong.GreenDAO.SmsDAO;
import com.deginer.fithou.vumanhduc.yeuthuong.ListSmsActivity;
import com.deginer.fithou.vumanhduc.yeuthuong.MainActivity;
import com.deginer.fithou.vumanhduc.yeuthuong.Models.Sms;
import com.deginer.fithou.vumanhduc.yeuthuong.R;

import java.util.ArrayList;
import java.util.List;


public class ListSmsFragment extends Fragment {

    ListView lv_sms;
    SmsDAO smsDAO;
    SmsAdapter adapter;
    FloatingActionButton fabb;
    int type;
     ArrayList<Sms> smses = new ArrayList<Sms>();
    private MenuInflater menuInflater;

    public ListSmsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        ((ListSmsActivity)getActivity()).getSupportActionBar().setHomeButtonEnabled(true);
        ((ListSmsActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((ListSmsActivity)getActivity()).getSupportActionBar().setTitle(hientitle());

        View view = inflater.inflate(R.layout.fragment_list_sms, container, false);

        lv_sms = (ListView) view.findViewById(R.id.lv_sms);
        smsDAO = new SmsDAO(getActivity());
        Intent intent = getActivity().getIntent();
        String id = intent.getStringExtra(Intent.EXTRA_TEXT);
        type= Integer.parseInt(id);

        //Đăng kí context menu cho lv
        registerForContextMenu(this.lv_sms);

        if (id.equals("10") != true) {
            smses = smsDAO.findByType(type);
            adapter = new SmsAdapter(getActivity(), smses);

            lv_sms.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    showDialogIntent(Html.fromHtml(smses.get(position).getContent()));
                }
            });

        } else {
            final ArrayList<Sms> smsBookmark = smsDAO.findByBoorkmark();
            adapter = new SmsAdapter(getActivity(), smsBookmark);
            lv_sms.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    showDialogIntent(Html.fromHtml(smsBookmark.get(position).getContent()));
                }
            });

            lv_sms.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                    new AlertDialog.Builder(getActivity())
                            .setTitle("Bỏ yêu thích")
                            .setPositiveButton("Hủy", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .setNegativeButton("Bỏ yêu thích", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    smsDAO.removeBookmark(smsBookmark.get(position).getId());
                                    smsBookmark.remove(smsBookmark.get(position));
                                    adapter.notifyDataSetChanged();
                                }
                            })
                            .show();
                    return true;
                }
            });
        }
        fabb = (FloatingActionButton) view.findViewById(R.id.fab_add);
        fabb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                themloichuc();

            }
        });

        lv_sms.setAdapter(adapter);
        getActivity().setTitle(intent.getStringExtra("title"));
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    private void themloichuc() {
        //
//                final Dialog dialog = new Dialog(getActivity());
//                dialog.setTitle("Thêm lời chúc");
//                dialog.setContentView(R.layout.activity_themtinnhan);
        final android.support.v7.app.AlertDialog.Builder mBuilder = new android.support.v7.app.AlertDialog.Builder(getActivity());
        View mview = getActivity().getLayoutInflater().inflate(R.layout.activity_themtinnhan,null);
        final EditText noidungtin = (EditText) mview.findViewById(R.id.et_them);
        Button buttonluu = (Button) mview.findViewById(R.id.btthem);
        Button buttonhuy = (Button) mview.findViewById(R.id.bthuy);
        mBuilder.setView(mview);
        final android.support.v7.app.AlertDialog dialog = mBuilder.create();
        dialog.setCancelable(false);
        dialog.show();
        buttonhuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });
        buttonluu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (noidungtin.length() < 10) {
                    Toast.makeText(getContext(),
                            "Nội dung quá ngắn!", Toast.LENGTH_SHORT).show();
                } else {
                    smsDAO.addSms(type,noidungtin.getText().toString());
                    refreshSms();
                    Toast.makeText(getContext(),
                            "Thêm thành công " + noidungtin.getText().toString(), Toast.LENGTH_LONG).show();
                    dialog.cancel();
                }
            }
        });
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getActivity().getMenuInflater().inflate(R.menu.menu_selected_team,menu);
        menu.setHeaderTitle("Chọn hành động");
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo  info =(AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
        final Sms sms = (Sms) this.lv_sms.getItemAtPosition(info.position);
        switch (item.getItemId()) {
            case R.id.menu_thich:
            {
                if (sms.getBookmart() != 1) {
                        new AlertDialog.Builder(getActivity())
                                .setTitle("Thêm vào yêu thích")
                                .setPositiveButton("Hủy", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                })
                                .setNegativeButton("Thêm", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        smsDAO.addBookmark(sms.getId());
                                        sms.setBookmart(1);
                                        adapter.notifyDataSetChanged();
                                    }
                                })
                                .show();
                    } else {
                        new AlertDialog.Builder(getActivity())
                                .setTitle("Bỏ yêu thích")
                                .setPositiveButton("Hủy", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                })
                                .setNegativeButton("Bỏ yêu thích", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        smsDAO.removeBookmark(sms.getId());
                                        sms.setBookmart(0);
                                        adapter.notifyDataSetChanged();
                                    }
                                })
                                .show();
                    }
                    return true;
                }
            case R.id.menu_sua:
                Toast.makeText(getActivity(), sms.getId()+"", Toast.LENGTH_LONG).show();
                return true;
            case R.id.menu_xoa:
            {
                // Hỏi trước khi xóa.
                new android.support.v7.app.AlertDialog.Builder(getContext())
                        .setMessage("Are you sure you want to delete "+sms.getContent()+" ?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                xoaloichuc(sms);
                            }
                        })
                        .setNegativeButton("No", null)
                        .show();
            }


            return  true;

        }
        return super.onOptionsItemSelected(item);
    }

    private void xoaloichuc(Sms sms) {
        smsDAO.deleteTeam(sms);
        this.smses.remove(sms);
        this.adapter.notifyDataSetChanged();
        Toast.makeText(getActivity(), "Xóa thành công!", Toast.LENGTH_LONG).show();
    }


    public void refreshSms(){
        this.smses.clear();
        List<Sms> list = smsDAO.findByType(type);
        this.smses.addAll(list);
        this.adapter.notifyDataSetChanged();
    }

    private String hientitle() {
        String title;
        Intent intent = getActivity().getIntent();
        title = intent.getStringExtra("title").toString();
        return title;
    }

    @SuppressWarnings("deprecation")
    public void copyToClipboard(Spanned text) {
        ClipboardManager clipboardManager = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
        clipboardManager.setText(text);
        Toast.makeText(getActivity(), "Đã sao chép", Toast.LENGTH_SHORT).show();
    }

    public void shareIntent(Spanned text) {
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_TEXT, text.toString());
        startActivity(Intent.createChooser(shareIntent, "Send with"));
    }

    public void showDialogIntent(final Spanned text) {
        new AlertDialog.Builder(getActivity())
                .setTitle("Tùy chọn")
                .setPositiveButton("Sao chép", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        copyToClipboard(text);
                    }
                })
                .setNegativeButton("Gửi", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        shareIntent(text);
                    }
                })
                .show();
    }

}
