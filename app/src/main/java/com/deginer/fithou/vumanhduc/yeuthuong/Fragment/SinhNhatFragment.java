package com.deginer.fithou.vumanhduc.yeuthuong.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.deginer.fithou.vumanhduc.yeuthuong.MainActivity;
import com.deginer.fithou.vumanhduc.yeuthuong.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class SinhNhatFragment extends Fragment {


    public SinhNhatFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        ((MainActivity)getActivity()).getSupportActionBar().setTitle("Chúc mừng sinh nhật");
        return inflater.inflate(R.layout.fragment_sinh_nhat, container, false);
    }

}
