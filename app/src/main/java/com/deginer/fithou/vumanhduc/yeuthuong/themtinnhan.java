package com.deginer.fithou.vumanhduc.yeuthuong;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.deginer.fithou.vumanhduc.yeuthuong.Adapters.SmsAdapter;
import com.deginer.fithou.vumanhduc.yeuthuong.Fragment.ListSmsFragment;
import com.deginer.fithou.vumanhduc.yeuthuong.GreenDAO.SmsDAO;
import com.deginer.fithou.vumanhduc.yeuthuong.Models.Sms;

public class themtinnhan extends AppCompatActivity {

    EditText tinnhan;
    ListView lv_sms;
    SmsDAO smsDAO;
    SmsAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_themtinnhan);
////

    }

    public void them (View view) {

        tinnhan=(EditText)this.findViewById(R.id.et_them);
        String tn = tinnhan.getText().toString();

        smsDAO = new SmsDAO(this);
//        smsDAO.addSms(tn,idnew);

        Toast.makeText(getApplicationContext(),
                "Thêm thành công "+tn, Toast.LENGTH_LONG).show();

        Intent quaylai = new Intent(this, ListSmsFragment.class);
        this.startActivity(quaylai);


    }

    public void cancle (View view) {
        onBackPressed();


    }
}
