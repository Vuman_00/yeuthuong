package com.deginer.fithou.vumanhduc.yeuthuong.Fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;

import com.deginer.fithou.vumanhduc.yeuthuong.Adapters.TitleAdapter;
import com.deginer.fithou.vumanhduc.yeuthuong.GreenDAO.TitleDAO;
import com.deginer.fithou.vumanhduc.yeuthuong.GreenDAO.TitleHelper;
import com.deginer.fithou.vumanhduc.yeuthuong.ListSmsActivity;
import com.deginer.fithou.vumanhduc.yeuthuong.MainActivity;
import com.deginer.fithou.vumanhduc.yeuthuong.Models.Title;
import com.deginer.fithou.vumanhduc.yeuthuong.R;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class ChucTetFragment extends Fragment {
    private View view;
    private GridView gridView;
    private TitleAdapter adapter;
    private TitleDAO titleDAO;
    public ChucTetFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        ((MainActivity)getActivity()).getSupportActionBar().setTitle("Chúc tết");
        return inflater.inflate(R.layout.fragment_chuc_tet, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        gridView =(GridView) getActivity().findViewById(R.id.list_title);
        titleDAO = new TitleDAO(getActivity());
        final ArrayList<Title> titles = titleDAO.getList();
        adapter = new TitleAdapter(getActivity(),titles);
        gridView.setAdapter(adapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getActivity(), ListSmsActivity.class);
                intent.putExtra(Intent.EXTRA_TEXT, titles.get(position).getId()+"");
                intent.putExtra("title", titles.get(position).getTitle() + "");
                startActivity(intent);
            }
        });
    }


}
