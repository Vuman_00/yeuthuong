package com.deginer.fithou.vumanhduc.yeuthuong.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.deginer.fithou.vumanhduc.yeuthuong.Models.Title;
import com.deginer.fithou.vumanhduc.yeuthuong.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by VuManhDuc on 26/03/2017.
 */

public class TitleAdapter extends ArrayAdapter<Title> {
    private Context context;
    private ArrayList<Title> titles;

    public TitleAdapter(Context context, ArrayList<Title> titles) {
        super(context,R.layout.item_title,titles);
        this.context=context;
        this.titles=titles;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater= (LayoutInflater) context
                .getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.item_title,parent,false);
        TextView tv_title = (TextView)view.findViewById(R.id.tv_title);
        tv_title.setText(titles.get(position).getTitle());
        return  view;

    }
}
