package com.deginer.fithou.vumanhduc.yeuthuong.Adapters;


import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.deginer.fithou.vumanhduc.yeuthuong.Models.Sms;
import com.deginer.fithou.vumanhduc.yeuthuong.R;

import java.util.ArrayList;

/**
 * Created by truongpq on 10/8/15.
 */
public class SmsAdapter extends ArrayAdapter<Sms> {
    private Context context;
    private ArrayList<Sms> smses;

    public SmsAdapter(Context context, ArrayList<Sms> smses) {
        super(context, R.layout.item_sms, smses);
        this.context = context;
        this.smses = smses;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.item_sms, parent,false);
        TextView tv_sms = (TextView) view.findViewById(R.id.tv_sms);
        tv_sms.setText(Html.fromHtml(smses.get(position).getContent()));
        if (smses.get(position).getBookmart() == 1) {
            ImageView img_bookmark = (ImageView) view.findViewById(R.id.img_bookmark);
            img_bookmark.setImageResource(R.drawable.ic_star);
        }
        return view;
    }
}