package com.deginer.fithou.vumanhduc.yeuthuong;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.deginer.fithou.vumanhduc.yeuthuong.Fragment.ChucTetFragment;
import com.deginer.fithou.vumanhduc.yeuthuong.Fragment.HomeFragment;
import com.deginer.fithou.vumanhduc.yeuthuong.Fragment.SinhNhatFragment;
import com.deginer.fithou.vumanhduc.yeuthuong.GreenDAO.TitleHelper;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

       /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        */

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        /////////////////////


        ChucTetFragment tetFragment = new ChucTetFragment();
        FragmentManager manager = getSupportFragmentManager();
        manager.beginTransaction().replace(R.id.content_main,tetFragment,tetFragment.getTag()).commit();

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            new AlertDialog.Builder(this).setIcon(android.R.drawable.ic_dialog_alert).setTitle("Thoát")
                    .setMessage("Bạn có chắc chắn muốn thoát?")
                    .setPositiveButton("Thoát", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                            System.exit(0);
                        }
                    }).setNegativeButton("Hủy", null).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

            switch (id) {
                case R.id.guide:
                    new AlertDialog.Builder(this).setIcon(android.R.drawable.ic_menu_help).setTitle("Hướng dẫn")
                            .setMessage("Chạm để tùy chọn gửi hoặc sao chép, giữ để thêm hoặc bỏ yêu thích.")
                            .show();
                    break;
                case R.id.introduce:
                    new AlertDialog.Builder(this).setIcon(android.R.drawable.ic_dialog_info).setTitle("Giới thiệu")
                            .setMessage("Những mẫu tin nhắn yêu thương giúp bạn bày tỏ tình cảm với những người thân yêu.")
                            .show();
                    break;
                case R.id.contact:
                    new AlertDialog.Builder(this).setIcon(android.R.drawable.stat_sys_speakerphone).setTitle("Liên hệ")
                            .setMessage("Họ tên: Vũ Mạnh Đức \n Email: Vumanhduca2@gmail.com")
                            .show();
                    break;
                case R.id.action_settings:
                    break;
            }

            return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            HomeFragment homeFragment = new HomeFragment();
            FragmentManager manager = getSupportFragmentManager();
            manager.beginTransaction().replace(R.id.content_main,homeFragment,homeFragment.getTag()).commit();
            // Handle the camera action
        } else if (id == R.id.nav_chuctet) {
            ChucTetFragment tetFragment = new ChucTetFragment();
            FragmentManager manager = getSupportFragmentManager();
            manager.beginTransaction().replace(R.id.content_main,tetFragment,tetFragment.getTag()).commit();

        } else if (id == R.id.nav_sinhnhat) {
            SinhNhatFragment sinhNhatFragment = new SinhNhatFragment();
            FragmentManager manager = getSupportFragmentManager();
            manager.beginTransaction().replace(R.id.content_main,sinhNhatFragment,sinhNhatFragment.getTag()).commit();
        } else if (id == R.id.nav_ngaylekhac) {

        } else if (id == R.id.nav_chiase) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

}
