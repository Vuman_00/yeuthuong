package com.deginer.fithou.vumanhduc.yeuthuong.GreenDAO;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;


import com.deginer.fithou.vumanhduc.yeuthuong.Models.Title;

import java.util.ArrayList;

/**
 * Created by Vu Manh Duc on 24/03/17
 */

public class TitleDAO {
    public static final String TABLE_NAME = "tb_cat";

    public static final String KEY_ID = "id";
    public static final String KEY_TITLE = "name";

    SQLiteDatabase db;
    TitleHelper helper;

    public TitleDAO(Context context) {
        helper = new TitleHelper(context);
        helper.createDB();
        db = helper.openDB();
    }

    public ArrayList<Title> getList() {
        ArrayList<Title> notes = new ArrayList<>();

        String selectQuerry = "SELECT " +
                KEY_ID + ", " +
                KEY_TITLE +
                " FROM " + TABLE_NAME;

        Cursor cursor = db.rawQuery(selectQuerry, null);

        if (cursor.moveToFirst()) {
            do {
                Title title = new Title();
                title.setId(cursor.getInt(0));
                title.setTitle(cursor.getString(1));
                notes.add(title);
            } while (cursor.moveToNext());
        }
        cursor.close();

        return notes;
    }
}
